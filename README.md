![](https://gitlab.com/uploads/-/system/project/avatar/45799609/password-front.png?width=64)
# Auth Service Front  
Angular app for generic authentication service  

![Angular](https://img.shields.io/badge/Angular-DD0031?style=for-the-badge&logo=angular&logoColor=white)
![TypeScript](https://img.shields.io/badge/TypeScript-007ACC?style=for-the-badge&logo=typescript&logoColor=white)
![NodeJs](https://img.shields.io/badge/Node.js-43853D?style=for-the-badge&logo=node.js&logoColor=white)
![GitLab](https://img.shields.io/badge/gitlab-%23fb8500.svg?style=for-the-badge&logo=gitlab&logoColor=white)
![Docker](https://img.shields.io/badge/docker-%230db7ed.svg?style=for-the-badge&logo=docker&logoColor=white)
![Firebase](https://img.shields.io/badge/firebase-%23ffb703.svg?style=for-the-badge&logo=firebase&logoColor=black)
  
> _Status:_ **🚧  Auth Service Front 🚀 Development  🚧**  
  
## Tools and technology
The following technologies and frameworks were used to develop the project:  
* [Angular](https://angular.io/)
* [NodeJS](https://nodejs.org/en)  
* [Angular CLI](https://angular.io/cli)
* [GitLab](https://about.gitlab.com/)
* [Docker](https://docs.docker.com/)
* [Firebase](https://firebase.google.com/)
  
## Features
### Implemented features
This App provides the following functionalitys: 


## Getting started
Before starting, you will need to have the following tools installed on your machine:
* [NodeJS](https://nodejs.org/en)
* [NPM](https://docs.npmjs.com/getting-started)
* [Angular CLI](https://angular.io/cli)
  
Also it's nice to have an editor to work with the code like  [VSCode](https://code.visualstudio.com/).
  
## Quick start
```
# Clone this repository
$ git clone https://gitlab.com/josemar.souza/auth-service-front.git

# Access project folder in terminal/cmd
$ cd auth-service-front

# Install dependencies
$ npm install

# Run the API 
$ npm start

# The server will start on port:4200 
# Access http://localhost:4200 to browse the app.
```    
## Run with docker image
You need to have Docker installed on your machine. Access this tutorial to learn how to install docker [https://docs.docker.com/engine/install/ubuntu/](https://docs.docker.com/engine/install/ubuntu/)
```
# Pull the image from DockerHub
$ docker pull jmsouzadev/auth-service-front

# Run the container
$ docker run -p 9000:9000 auth-service-api

# The server will start on port:9000 
# Access http://localhost:9000 to browse the app.
``` 
## Next steps
To improve the project and complete all future features, you will need:
* Expand test coverage
* Document the code